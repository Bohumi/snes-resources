;============================================================================
; Macros
;============================================================================

;============================================================================
;LoadPalette - Macro that loads palette information into CGRAM
;----------------------------------------------------------------------------
; In: SRC_ADDR -- 24 bit address of source data, 
;     SIZE -- # of COLORS to copy
;---------------------------------------------------------------------------- 
; Out: None
;----------------------------------------------------------------------------
; Modifies: -
; Requires: -
;----------------------------------------------------------------------------
.MACRO LoadPalette
	php
	phb
	
	pha
	phx
	phy
	
	sep #$20
	
    stz $2121       ; Start at START color
    lda #:\1        ; Using : before the parameter gets its bank.
    ldx #\1         ; Not using : gets the offset address.
    ldy #(\2 * 2)   ; 2 bytes for every color
    
    
    stx $4302   ; Store data offset into DMA source offset
    sta $4304   ; Store data bank into DMA source bank
    sty $4305   ; Store size of data block

    stz $4300   ; Set DMA Mode (byte, normal increment)
    lda #$22    ; Set destination register ($2122 - CGRAM Write)
    sta $4301
    lda #$01    ; Initiate DMA transfer
    sta $420B
	
	ply
	plx
	pla
	
	plb
	plp
.ENDM




;============================================================================
;LoadTable - Macro that loads a complete table into VRAM
;----------------------------------------------------------------------------
; In: TABLE_SRC_ADDRESSE -- 8 bit address of source data
;---------------------------------------------------------------------------- 
; Out: None
;----------------------------------------------------------------------------
; Modifies: -
; Requires: VRAM has to be initialized
;----------------------------------------------------------------------------
.MACRO LoadTable
	php
	pha
	phy
	phx
	
	ldx #$01
-
	
	clc
	adc X
	lda \1,x
	sta $2118
	txa
	inx
	ldy #$00
	cmp \1,y
	bne -

	plx
	ply
	pla
	plp
.ENDM