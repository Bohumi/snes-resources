.INCLUDE "header.inc"
.INCLUDE "init.asm"
.INCLUDE "macros.asm"


.bank 0 slot 0
.org 0
.section "Main"
.INCLUDE "ownfiles/mymacros.asm"
;--------------------------------------
Start:
	InitSNES
	
	rep #%00010000	;16 bit xy
	sep #%00100000	;8 bit ab

	ldx #$0000
	
	LoadPalette UntitledPalette $04
	

	ldx #UntitledData	; Address
	lda #:UntitledData	; of UntitledData
	  
	ldy #$0400  		; length of data
	  
	
	stx $4302			; write 
	sta $4304			; address
	sty $4305			; and length
	lda #%00000001		; set this mode (transferring words)
	sta $4300
	lda #$18	; $211[89]: VRAM data write
	sta $4301	; set destination
	ldy #$0000	; Write to VRAM from $0000
	sty $2116
	lda #%00000001	; start DMA, channel 0
	sta $420B
	    
	lda #%00000000	; VRAM writing mode
	sta $2115
	ldx #$4000	; write to vram
	stx $2116	; from $4000
 
 
		ldx #$01
	-
		lda UntitledTable.l,x
		sta $2118
		txa
		inx
		ldy $00
		cmp UntitledTable.l,y
		bne -
		
	
lda #%10000000	; VRAM writing mode
	sta $2115
	
	;set up the screen
	lda #%00110000	; 16x16 tiles, mode 0
	sta $2105	; screen mode register
	lda #$0040	; data starts from $4000
	sta $2107	; for BG1

	stz $210B	; BG1 and BG2 use the $0000 tiles

	lda #%00000001	; enable bg1
	sta $212C

	;The PPU doesn't process the top line, so we scroll down 1 line. (wtf???)
	rep #$20	; 16bit a
	lda #$07FF	; this is -1 for BG1
	sep #$20	; 8bit a
	sta $210E	; BG1 vert scroll
	
	sta $210e


	lda #%00001111	; enable screen, set brightness to 15
	sta $2100
- jmp -
.ends
;============================================================================
; Character Data
;============================================================================
.BANK 1 SLOT 0
.ORG 0
.SECTION "CharacterData"

    .INCLUDE "images/big.pcx.inc"

.ENDS